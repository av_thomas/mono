const tracked = require('./devops/tracking.json')
const setup = require('./test/setup.json')
const fs = require('fs')

const changed = process.env.npm_config_changed
const type = process.env.npm_config_type

console.log(type)
console.log(changed.split(''))

const updated = type === 'deploy' ? tracked : setup
updated.changed = changed.split(',')

const writeTo =
  type === 'deploy' ? './devops/tracking.json' : './test/setup.json'
console.log('writeTo', writeTo)
console.log('updated', updated)
fs.writeFile(writeTo, JSON.stringify(updated), err => {
  if (err) {
    console.log('Error writing file', err)
  } else {
    console.log('Successfully wrote file')
  }
})
